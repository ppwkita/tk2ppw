from django.urls import path,include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import url

app_name='add'

urlpatterns = [
    path('', views.get_schedule, name='adds'),
    path('django/', views.get_json, name = 'json'),
]
urlpatterns += staticfiles_urlpatterns()
