from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from book.models import booking
from apptk.models import mainUser

# Create your views here.
def history(request):
	if request.user.is_authenticated:
		obj=mainUser.objects.get(user=request.user)
		daftar_parkir = {"daftar_parkir":booking.objects.filter(milik=obj)}
		return render(request,'hist.html',daftar_parkir)
	return render(request,'hist.html')