from django.shortcuts import render,redirect
from .forms import formU,formPenting
from .models import mainUser

from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login,logout,authenticate

def home(request):
    return render(request,'hello.html')

def signin(request):
    if request.method=="POST":
        form=AuthenticationForm(request,data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username','')
            password = form.cleaned_data.get('password','')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request,user)
                messages.info(request,f"Congratulations, you're login now {username}")
                return render(request,'hello.html',{"message":username})
            else:
                messages.error(request,"There is some error in username or password")

        else:
            messages.error(request,"There is some error in username or password")
    
    form=AuthenticationForm()
    return render(request,'signin.html',{"form":form})

def regis(request):
    if(request.method == "POST"):
        form=formPenting(request.POST)
        profile=formU(request.POST)
        if form.is_valid() and profile.is_valid():
            user =form.save(commit=False)
            username = form.cleaned_data.get('username','')
            email=form.cleaned_data.get('email','')
            password = form.cleaned_data.get('password','')
            print('masuk sini')
            user.set_password(password)
            user.save()
            akun=profile.save(commit=False)
            akun.user=user
            akun.save()
            return redirect("apptk:signup")
        else:
            return redirect("/")
        
    form=formU()
    formUse=formPenting()
    context={
        'formUser':formUse,
        'formAcc':form,
    }

    return render(request,'signup.html',context)

def logout_req(request):
    logout(request)
    return redirect("apptk:signin")