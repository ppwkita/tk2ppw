from django import forms
from .models import mainUser
from django.contrib.auth.models import User

class formPenting(forms.ModelForm):
    username=forms.CharField(widget=forms.TextInput(attrs={'placeholder' :'Masukan Username','id':'forms','id':'username'}))
    email=forms.CharField(widget=forms.TextInput(attrs={'placeholder' :'Masukan E-mail','class':'forms','id':'email'}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'placeholder' :'Masukan Password','class':'forms','id':'password'}))
    class Meta:
        model=User
        fields=['username','email','password']

class formU(forms.ModelForm):
    namaPanggil=forms.CharField(max_length=30,label="Nama Panggilan",widget=forms.TextInput(attrs={'placeholder':'Masukkan nama panggil anda','id':'namaPanggil'}))
    class Meta:
        model=mainUser
        fields=['namaPanggil']

