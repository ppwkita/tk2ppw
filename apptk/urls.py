from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name='apptk'

urlpatterns = [
    path('', views.home, name='home'),
    path('signin/', views.signin, name='signin'),
    path('signup/',views.regis,name='signup'),
    path('logout/',views.logout_req,name='logout'),
    
]

urlpatterns += staticfiles_urlpatterns()
