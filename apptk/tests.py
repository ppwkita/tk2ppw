from django.test import TestCase, Client
from django.urls import resolve
from .views import home,signin,regis

# Create your tests here.
class test_Unit(TestCase):
    def test_add_url_is_exist(self):
        response= Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_add_url_is_exist2(self):
        response= Client().get('/signin/')
        self.assertEqual(response.status_code,200)

    def test_add_url_is_exist3(self):
        response= Client().get('/signup/')
        self.assertEqual(response.status_code,200)
        
    def test_template_exist2(self):
        response= Client().get('/signin/')
        self.assertTemplateUsed(response,"signin.html")

    def test_template_exist3(self):
        response= Client().get('/signup/')
        self.assertTemplateUsed(response,"signup.html")

    def test_function_signup(self):
        found=resolve("/signup/")
        self.assertEqual(found.func,regis)
        

    def test_template2_exist(self):
        response= Client().get('')
        self.assertTemplateUsed(response,"hello.html")
        
    def test_file_render(self):
        found=resolve("/signin/")
        self.assertEqual(found.func,signin)

    def test_file2_render(self):
        found=resolve("/")
        self.assertEqual(found.func,home)