from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name='book'

urlpatterns = [
    path('', views.book, name='book'),
    path('django/', views.get_json, name = 'json'),
    
]

urlpatterns += staticfiles_urlpatterns()
