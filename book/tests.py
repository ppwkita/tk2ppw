from django.test import TestCase, Client
from django.urls import resolve
from .views import book
from .models import booking
from apptk.models import mainUser

class test_Unit(TestCase):
    def test_add_url_is_exist(self):
        response= Client().get('/book/')
        self.assertEqual(response.status_code,200)

    def test_template_exist(self):
        response= Client().get('/book/')
        self.assertTemplateUsed(response,"book.html")
        
    def test_file_render(self):
        found=resolve("/book/")
        self.assertEqual(found.func,book)

    
    
# Create your tests here.
