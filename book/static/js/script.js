$(function() {

    $('#id_nama_tempat').keyup(function() {
        console.log($('#id_nama_tempat').val())
        $.ajax({
            type: "GET",
            url: "search/",
            data: {
                'search_text' : $('#id_nama_tempat').val(),
                'csrfmiddlewaretoken' : $("#id_nama_tempat[name=csrfmiddlewaretoken]").val()
            },
            success: searchSuccess,
            dataType: 'html'
        });
    });
});

function searchSuccess(data, textStatus, jqXHR)
{
    $('#search-results').html(data)
}