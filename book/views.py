from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect,JsonResponse
from add.models import daftar
from .forms import bookForm
from .models import booking
from apptk.models import mainUser
import datetime
import json


def book(request):
    if request.method == 'POST':
        formB = bookForm(request.POST)
        now = datetime.datetime.now()
        if formB.is_valid():

            now = datetime.datetime.now()
            hasilBook = booking(
                                lokasi = formB.data['lokasi'],
                                jam=str(now.hour)+":"+str(now.minute),                    
                                nama_tempat = formB.data['nama_tempat'],
                                milik=mainUser.objects.get(user=request.user))

                                
            print("sukses")
            hasilBook.save()
            
        return redirect('/book/')
    else:
        formB = bookForm()
        
    return render(request, 'book.html', {'formB':formB})
    
def get_json(request):
    db=daftar.objects.all()
    data_string='{"stasiun":['
    for i in db:
        strings=str(i.display_nama_tempat_parkir)
        data_string+='{"obj":"'+strings+'"},'
        # {"code":"AKB","location":"Aekloba, Labuhan Batu"},
    
    data_string=data_string[:-1]
    data_string+="]}"
    data_json = json.dumps(data_string)
    return HttpResponse(data_json, content_type="application/json")